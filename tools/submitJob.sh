#!/bin/bash
# First argument: compiler -- gnumpi, gnumva, intelmpi, intelmva, pgimpi, pgimva
# Second argument: year -- 2010, 2012, 2013, 2014
# Third argument: number of processors -- 4, 16, 64, 144
# Fourth argument: estimated wall time (optional, default is 02:00:00) -- recommended times:
#           4 processors: 02:00:00
#          16 processors: 02:00:00
#          64 processors: 01:00:00
#         144 processors: 00:30:00
# Example submission: ./submitJob.sh gnumpi 2010 4 02:00:00

if [ "$#" -lt 3 ] || [ "$#" -gt 4 ] ;
then
   echo ''
   echo 'Need the 3 or 4 input parameters -- compiler, year, #processors, estimated wall time (optional)'
   echo 'Exiting script.'
   echo ''
   exit
fi

wallTime=02:00:00
if [ "$#" == 4 ] ;
then
   wallTime=$4
fi

if [ "$1" = "gnumpi" ];
then
   compilerName=GNU_OPENMPI
   moduleName=gnu-openmpi
elif [ "$1" = "gnumva" ];
then
   compilerName=GNU_MVAPICH2
   moduleName=gnu-mvapich2
elif [ "$1" = "intelmpi" ];
then
   compilerName=INTEL_OPENMPI
   moduleName=intel-openmpi
elif [ "$1" = "intelmva" ];
then
   compilerName=INTEL_MVAPICH2
   moduleName=intel-mvapich2
elif [ "$1" = "pgimpi" ];
then
   compilerName=PGI_OPENMPI
   moduleName=pgi-openmpi
elif [ "$1" = "pgimva" ];
then
   compilerName=PGI_MVAPICH2
   moduleName=pgi-mvapich2
fi

module purge
module load $moduleName

#When editing userOutDir, make sure the '/' is not included at the end of the name
execDir=/panfs/storage.local/src/benchmarks/benchmarks/$compilerName/test/em_real
userOutDir=$execDir/output
outDir=$userOutDir/$1_$2_$3
#outMSUB=$outDir/$1_$2_$3.msub
outSLURM=$outDir/$1_$2_$3.slurm
executable=$execDir/wrf.exe
queue=backfill

echo ''
echo ''

if [ -d $outDir ];
then
   echo 'Deleting existing directory:'
   echo '  '$outDir
   rm -rf $outDir
fi

echo 'Creating directory:' 
echo '  '$outDir
mkdir $outDir


#touch $outMSUB
#
##Generate MOAB script
#echo '#!/bin/bash' >> $outMSUB
#echo '#MOAB -N "job_'$1'_'$2'_'$3'"' >> $outMSUB
#echo '#MOAB -l nodes='$3 >> $outMSUB
#echo '#MOAB -j oe' >> $outMSUB
#echo '#MOAB -q '$queue >> $outMSUB
#echo '#MOAB -l walltime='$wallTime >> $outMSUB
#echo '#MOAB -l feature=YEAR'$2 >> $outMSUB
#echo '#MOAB -d '$outDir >> $outMSUB
#echo '' >> $outMSUB
#
#echo 'module purge' >> $outMSUB
#echo 'module load '$moduleName >> $outMSUB
#
#echo 'mpiexec -np '$3'' ''$executable >> $outMSUB
#
#echo 'Generated msub script:'
#echo '  '$outSLURM


touch $outSLURM
#Generate SLURM script
echo '#!/bin/bash' >> $outSLURM
echo '#' >> $outSLURM
echo '#SBATCH --job-name="job_'$1'_'$2'_'$3'"' >> $outSLURM
echo '#SBATCH -N '$3 >> $outSLURM
#echo '#SBATCH [output/error]
echo '#SBATCH -p '$queue >> $outSLURM
echo '#SBATCH -t '$wallTime >> $outSLURM
#echo '#SBATCH [feature year]
echo '#SBATCH -D '$outDir >> $outSLURM


echo 'module purge' >> $outSLURM
echo 'module load '$moduleName >> $outSLURM

echo 'mpiexec -np '$3'' ''$executable >> $outSLURM

echo 'Generated slurm script:'
echo '  '$outSLURM

echo 'Moving to output directory: ' 
echo '  '$outDir
cd $outDir

#Generate sybolic links for the output directory, but make sure not to include the output and namelist.input files.
echo 'Making symbolic links in output directory.'
findCommand=`find $execDir \( -name output -o -name namelist.input \) -prune -o -print`
 
for file in ${findCommand}
do
   name=`basename $file`
   ln -s $file $name;
done

#Add the namelist file instead of the symbolic linked namelist file, since it needs to be edited.
echo 'Copying relevant files.'
cp $execDir/namelist.input $outDir/

#Edit the namelist file to change nproc_x and nproc_y to reflect the number of processors used.
calc=$(echo "sqrt("$3")" | bc)
sed -i '55s/.*/ nproc_x                             = '"$calc"',/' namelist.input
sed -i '56s/.*/ nproc_y                             = '"$calc"',/' namelist.input

rm em_real

echo 'Submitting slurm script.'
sbatch $outMSUB
