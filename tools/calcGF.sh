#!/bin/bash
# All command-line options are in lower case.
# $1 = compiler -- gnumpi, gnumva, intelmpi, intelmva, pgimpi, pgimva
# $2 = year -- 2010, 2012, 2013, 2014
# $3 = number of processors -- 4, 16, 64, 144





if [ "$#" -ne 3 ];
then
   echo ''
   echo 'Need the 3 input parameters -- compiler, year, #processors'
   echo 'Exiting script.'
   echo ''
   exit
fi

#When editing, make sure not to include a `/` after the folder name
publishedResults=/panfs/storage.local/src/benchmarks/publish/results

if [ "$1" = "gnumpi" ];
then
   outDir=$publishedResults/GNU_OPENMPI/$1_$2_$3
elif [ "$1" = "gnumva" ];
then
   outDir=$publishedResults/GNU_MVAPICH2/$1_$2_$3
elif [ "$1" = "intelmpi" ];
then
   outDir=$publishedResults/INTEL_OPENMPI/$1_$2_$3
elif [ "$1" = "intelmva" ];
then
   outDir=$publishedResults/INTEL_MVAPICH2/$1_$2_$3
elif [ "$1" = "pgimpi" ];
then
   outDir=$publishedResults/PGI_OPENMPI/$1_$2_$3
elif [ "$1" = "pgimva" ];
then
   outDir=$publishedResults/PGI_MVAPICH2/$1_$2_$3
fi

echo 'Output directory:'
echo '  '$outDir

echo ''
echo 'Timing information for:'
echo '   Compiler: '$1
echo '   Year: '$2
echo '   Nodes: '$3
echo ''

#Command to find timing, found from WRF website
grep 'Timing for main:' $outDir/rsl.error.0000 | tail -149 | awk '{print $9}' | awk -f stats.awk
echo ''
