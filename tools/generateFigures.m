function plotResults
    
    %   Though the program will run with all of these set to 'true', 
    %   it may take a long time to output all the plots. 

    %Run time as a function of #processors for one compiler in all years
    allYears_avg = true;
    
    %Run time as a function of #processors for all compilers in one year
    allCompilers_avg = true;
    
    %Gflops as a function of #processors for one compiler in all years
    allYears_gflops = true;
    
    %Gflops as a function of #processors for all compilers in one year
    allCompilers_gflops = true;
 
    %    Plot attributes
    %Change these to modify the appearance of all the plots
    modifiers_allYears = {'k','b','r','g'};
    modifiers_allCompilers = {'k','--k','b','--b','r','--r'};
    lineWidth = 2;
    numberFontSize = 16;
    labelFontSize = 18;
    legendFontSize = 16;
    titleFontSize = 20;
    xScale = 'Linear';
    xLimits = [4 144];
    xTicks = [4 16 64 144];
    yScale = 'Log';
    
    %    Figure folder location (Make sure the `/` is at the end)
    figureFolder = '/panfs/storage.local/src/benchmarks/publish/figs/';
    
    fid = fopen('benchmarkResults.txt');
    out = textscan(fid,'%s %f %f %f %f');
    [compiler, year, processor, avg, gflops] = out{:,:,:,:,:};
    
    compilerNames = {'gnumpi','gnumva','intelmpi','intelmva','pgimpi','pgimva'};
    titleNames = {'GNU OPEN MPI','GNU MVAPICH2','INTEL OPEN MPI','INTEL MVAPICH2','PGI OPEN MPI','PGI MVAPICH2'};
    fileNames = {'GNU_OPENMPI','GNU_MVAPICH2','INTEL_OPENMPI','INTEL_MVAPICH2','PGI_OPENMPI','PGI_MVAPICH2'}; 
    years = [2010, 2012, 2013, 2014];

%----------------------------------------------------------------------
 
    compilerLength = length(compilerNames);
    yearsLength = length(years);

    plotNum = 1;     
    if (allYears_avg == true)
        %Run time as a function of #processors for one compiler in all years

        for i = 1:compilerLength
            compilerIndices = find(strcmp(compiler,compilerNames{i}));
            compilerResults = [year(compilerIndices),processor(compilerIndices),avg(compilerIndices)];
            
            %Plot data from benchmarkResults text file
            fig(plotNum) = figure('visible','off');
            hold on;
            box on;
            for j = 1:yearsLength
                tempIndices = find(compilerResults(:,1) == years(j));
                plotValues1 = compilerResults(tempIndices,2);
                plotValues2 = compilerResults(tempIndices,3);
                plotValues2(plotValues2 == 0) = nan; %Some of the benchmark tests are incomplete, indicated by a 0 value. These aren't included.
                plot(plotValues1,plotValues2,modifiers_allYears{j},'LineWidth',lineWidth);
            end
            
            %Plot attributes
            set(gca,'FontSize',numberFontSize);
            leg = legend(cellstr(num2str(years')),'Location','northeast');
            set(leg,'FontSize',legendFontSize);
            title(titleNames{i},'FontSize',titleFontSize);
            
            xlabel('Number of Processors','FontSize',labelFontSize);
            set(gca,'XLim',xLimits);
            set(gca,'XTick',xTicks);
            set(gca,'XScale',xScale);
            
            ylabel('Average run time','FontSize',labelFontSize);
            set(gca,'YScale',yScale);

            hold off;
            
            %Save in figure folder.
            saveas(fig(plotNum), strcat(figureFolder,'allYears_avg/',fileNames{i}),'png');

            plotNum = plotNum + 1;
        end
    end
    
    if (allCompilers_avg == true)
        %Run time as a function of #processors for all compilers in one year
        
        for i = 1:yearsLength
            yearIndices = find(year == years(i));
            compilerIndices = compiler(yearIndices);
            yearResults = [processor(yearIndices),avg(yearIndices)];
            
            fig(plotNum) = figure('visible','off');
            hold on;
            box on;
            for j = 1:compilerLength
                tempIndices = find(strcmp(compilerIndices(:),compilerNames{j}));
                plotValues1 = yearResults(tempIndices,1);
                plotValues2 = yearResults(tempIndices,2);
                plotValues2(plotValues2 == 0) = nan;
                plot(plotValues1,plotValues2,modifiers_allCompilers{j},'LineWidth',lineWidth);
            end
            set(gca,'FontSize',numberFontSize);
	    leg = legend(titleNames','Location','northeast');
	    set(leg,'FontSize',legendFontSize);
            title(int2str(years(i)),'FontSize',titleFontSize);
	    
            xlabel('Number of Processors','FontSize',labelFontSize);
            set(gca,'XLim',xLimits);
            set(gca,'XTick',xTicks);
            set(gca,'XScale',xScale);
	    
            ylabel('Average run time','FontSize',labelFontSize);
            set(gca,'YScale',yScale);
	    
            hold off;
            saveas(fig(plotNum), strcat(figureFolder,'allCompilers_avg/',int2str(years(i))),'png');   
       
            plotNum = plotNum + 1;
        end
    end
    
    if (allYears_gflops == true)
        %Gflops as a function of #processors for one compiler in all years

        for i = 1:compilerLength
            compilerIndices = find(strcmp(compiler,compilerNames{i}));
            compilerResults = [year(compilerIndices),processor(compilerIndices),gflops(compilerIndices)];
            
            fig(plotNum) = figure('visible','off');
            hold on;
            box on;
            for j = 1:yearsLength
                tempIndices = find(compilerResults(:,1) == years(j));
                plotValues1 = compilerResults(tempIndices,2);
                plotValues2 = compilerResults(tempIndices,3);
                plotValues2(plotValues2 == 0) = nan;
                plot(plotValues1,plotValues2,modifiers_allYears{j},'LineWidth',lineWidth);
            end
            set(gca,'FontSize',numberFontSize);
            leg = legend(cellstr(num2str(years')),'Location','southeast');
            set(leg,'FontSize',legendFontSize);
            title(titleNames{i},'FontSize',titleFontSize);

            xlabel('Number of Processors','FontSize',labelFontSize);
            set(gca,'XLim',xLimits);
            set(gca,'XTick',xTicks);
            set(gca,'XScale',xScale);
            
            ylabel('Gflops','FontSize',labelFontSize);
            set(gca,'YScale',yScale);

            hold off;
            saveas(fig(plotNum), strcat(figureFolder,'allYears_gflops/',fileNames{i}),'png');

            plotNum = plotNum + 1;
        end
    
    end
    
    if (allCompilers_gflops == true)
        %Gflops as a function of #processors for all compilers in one year
        
        for i = 1:yearsLength
            yearIndices = find(year == years(i));
            compilerIndices = compiler(yearIndices);
            yearResults = [processor(yearIndices),gflops(yearIndices)];
            
            fig(plotNum) = figure('visible','off');
            hold on;
            box on;
            for j = 1:compilerLength
                tempIndices = find(strcmp(compilerIndices(:),compilerNames{j}));
                plotValues1 = yearResults(tempIndices,1);
                plotValues2 = yearResults(tempIndices,2);
                plotValues2(plotValues2 == 0) = nan;
                plot(plotValues1,plotValues2,modifiers_allCompilers{j},'LineWidth',lineWidth);
            end
            set(gca,'FontSize',numberFontSize);
	    leg = legend(titleNames','Location','southeast');
	    set(leg,'FontSize',legendFontSize);
            title(int2str(years(i)),'FontSize',titleFontSize);
	    
            xlabel('Number of Processors','FontSize',labelFontSize);
            set(gca,'XLim',xLimits);
            set(gca,'XTick',xTicks);
            set(gca,'XScale',xScale);
	    
            ylabel('Gflops','FontSize',labelFontSize);
            set(gca,'YScale',yScale);
	    
            hold off;
            saveas(fig(plotNum), strcat(figureFolder,'allCompilers_gflops/',int2str(years(i))),'png');   
       
            plotNum = plotNum + 1;
        end
    end
end
