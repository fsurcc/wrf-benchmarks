#!/usr/bin/python

import subprocess
import sys

compilers = ["gnumpi","gnumva","intelmpi","intelmva","pgimpi","pgimva"]
years = [2010,2012,2013,2014]
processors = [4, 16, 64, 144]

f = open('benchmarkResults.txt','w')
prevGflops = -1;
avgTime = "1"; #This is left as a string intentionally

for compiler in compilers: 
    for year in years: 
        for processor in processors:

            # Use calcGF.sh and stats.awk to find average time spent per time step
            cmd = "./calcGF.sh" + " " + compiler + " " + str(year) + " " + str(processor) + " | awk '/mean/ {print $NF}' | awk 'NR == 1'"
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.wait()

            for line in iter(p.stdout.readline, ""):
                avgTime = line
            
            # Calculation specified by WRF to find GFlops
            gflops = 72.0 / float(avgTime) * 0.418

            # Format output with dashes or 0's if there is no data available
            outStr = compiler + " " + str(year) + " " + str(processor) + " " + avgTime.rstrip('\n') + " " + str(gflops)
            if prevGflops == gflops:
                outStrTerminal = compiler + " " + str(year) + " " + str(processor) + "\t--" + " " + "\t--" + "\t[Incomplete]"
                outStrFile = compiler + " " + str(year) + " " + str(processor) + " " + "0" + " " + "0"
            else:
                outStrTerminal = outStr
                outStrFile = outStr

            print outStrTerminal
            f.write(outStrFile)
            f.write('\n')
            prevGflops = gflops
f.close()
